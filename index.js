/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getUserInfo() {
		let promptFullName = prompt("What is your name?");
		let promptAge = prompt("How old are you?");
		let promptAddress = prompt("Where do you live?");

		alert("Thank you for your input!");

		console.log("Hello, " + promptFullName);
		console.log("You are " + promptAge + " years old.");
		console.log("You live in " + promptAddress);
	};

	getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function getFaveBands() {
		console.log("1. Flyleaf");
		console.log("2. Blink-182");
		console.log("3. John Denver");
		console.log("4. Autotelic");
		console.log("5. Skillet");
	}

	getFaveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function getFaveMovies() {
		console.log("1. The Schindler's List");
		console.log("Rotten Tomatoes Rating: 97%");
		console.log("2. The Chronicles of Narnia: The Lion, The Witch, and The Wardrobe");
		console.log("Rotten Tomatoes Rating: 75%");
		console.log("3. Wreck-It-Ralph");
		console.log("Rotten Tomatoes Rating: 87%");
		console.log("4. The Girl Who Leapt Through Time");
		console.log("Rotten Tomatoes Rating: 84%");
		console.log("5. Book of Eli");
		console.log("Rotten Tomatoes Rating: 47%");
	}

	getFaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
